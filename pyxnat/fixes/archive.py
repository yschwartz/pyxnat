project_fields = {
    'id': 'project_id',
    'secondary_id': 'project_label'
    }

subject_fields = {
    'ID': 'subject_id',
    'label': 'subject_label',
    'project': 'project_id'
    }

experiment_fields = {
    'ID': 'experiment_id',
    'label': 'experiment_label',
    'project': 'project_id',
    'subject_ID': 'subject_id',
    'xsiType': 'data_type'
    }

assessor_fields = {
    'ID': 'assessor_id',
    'label': 'assessor_label',
    'project': 'project_id',
    'session_ID': 'experiment_id',
    'session_label': 'experiment_label',
    'subject_ID': 'subject_id',
    'xsiType': 'data_type'
    }

reconstruction_fields = {
    'ID': ['reconstruction_id', 'reconstruction_label']
    }

scan_fields = {
    'ID': ['scan_id', 'scan_label'],
    'xsiType': 'data_type'
    }

resource_fields = {
    'xnat_abstractresource_id': 'resource_id',
    'element_name': 'data_type'
    }


def file_fields(data):
    for row in data:
        if 'Name' in row:
            row['file_id'] = row['URI'].split('files/')[1].split('/')
            row['file_label'] = row['Name']
            del row['Name']
        if 'file_content' in row:
            row['content'] = row['file_content']
            del row['file_content']
        if 'file_format' in row:
            row['format'] = row['file_format']
            del row['file_format']
        if 'file_tags' in row:
            row['tags'] = row['file_tags']
            del row['file_tags']
        if 'cat_ID' in row:
            row['resource_id'] = row['cat_ID']
            del row['cat_ID']
        if 'Size' in row:
            row['size'] = row['Size']
            del row['Size']


def translate(data, entity_type, inverse=False):
    if isinstance(data, dict):
        data = [data]
    fields_map = globals()['%s_fields' % entity_type]
    if hasattr(fields_map, '__call__'):
        fields_map(data)
        return
    if inverse:
        fields_map = dict(zip(fields_map.values(), fields_map.keys()))
    for row in data:
        for field in fields_map:
            if field in row:
                if isinstance(fields_map[field], (str, unicode)):
                    row[fields_map[field]] = row[field]
                else:
                    for new_field in fields_map[field]:
                        row[new_field] = row[field]
                del row[field]


def projects_xml(data):
    pass


def subjects_xml(data):
    pass


def experiments_xml(data):
    pass


def scans_xml(data):
    pass


def assessors_xml(data):
    pass


def reconstructions_xml(data):
    pass
