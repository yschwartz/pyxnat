

def result_fields(data_type, fields, data):
    keys = [f.replace(':', '_').replace('/', '_').lower()
            if not f.startswith(data_type) else f.split('/')[1].lower()
            for f in fields]
    fixes = dict(zip(keys, fields))
    for row in data:
        for field in fixes:
            if field in row:
                row[fixes[field]] = row[field]
                del row[field]
            else:
                print 'not match', field

        if 'session_id' in row:
            row['experiment_id'] = row['session_id']
            del row['session_id']

        if '%s/SESSION_ID' % data_type in row:
            row['experiment_id'] = row['%s/SESSION_ID' % data_type]

        if 'project' in row:
            row['project_id'] = row['project']
            del row['project']

        if '%s/PROJECT' % data_type in row:
            row['project_id'] = row['%s/PROJECT' % data_type]

    return data
