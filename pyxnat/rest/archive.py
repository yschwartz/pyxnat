def complex_file_id(f):
    def func(*args, **kwargs):
        file_id = kwargs.get('file_id')
        file_id = [file_id] if isinstance(file_id, (str, unicode)) else file_id
        kwargs.pop('file_id')
        return f(file_id=file_id, *args, **kwargs)
    return f

# ---------------------------------------------------------------------------
# Project Resources
# ---------------------------------------------------------------------------

def projects(resource):
    return resource('projects')

def project(resource, project_id):
    return resource('projects', project_id)

def project_accessibility(resource, project_id):
    return project(resource, project_id)('accessibility')

def project_accessibility_mode(resource, project_id, mode):
    return project(resource, project_id)('accessibility', mode)

def project_current_arc(resource, project_id):
    return project(resource, project_id)('current_arc')

def project_current_arc_sub_folder(resource, project_id, sub_folder_name):
    return project(resource, project_id)('current_arc', sub_folder_name)

@complex_file_id
def project_file(resource, project_id, file_id):
    return project(resource, project_id)('files', *file_id)

def project_prearchive(resource, project_id):
    return project(resource, project_id)('prearchive_code')

def project_prearchive_code(resource, project_id, code):
    return project(resource, project_id)('prearchive_code', code)

def project_quarantine(resource, project_id):
    return project(resource, project_id)('quarantine_code')

def project_quarantine_code(resource, project_id, code):
    return project(resource, project_id)('quarantine_code', code)

def project_resources(resource, project_id):
    return project(resource, project_id)('resources')

def project_resource(resource, project_id, resource_id):
    return project(resource, project_id)('resources', resource_id)

# undocumented in XNAT
def project_resource_files(resource, project_id, resource_id):
    return project(resource, project_id)('resources', resource_id, 
                                                 'files')

@complex_file_id
def project_resource_file(resource, project_id, resource_id, file_id):
    return project(resource, project_id)('resources', resource_id, 
                                                 'files', *file_id)

# Project Resources: User Management

def project_users(resource, project_id):
    return project(resource, project_id, 'users')

def project_collaborator(resource, project_id, user_id):
    return project(resource, project_id, 'users', 'Collaborators', user_id)

def project_member(resource, project_id, user_id):
    return project(resource, project_id, 'users', 'Members', user_id)

def project_owner(resource, project_id, user_id):
    return project(resource, project_id, 'users', 'Owners', user_id)

# ---------------------------------------------------------------------------
# Project Resources: Project/Subject/Experiment Management
# ---------------------------------------------------------------------------

def project_subjects(resource, project_id):
    return project(resource, project_id)('subjects')

def project_subject(resource, project_id, subject_id):
    return project(resource, project_id)('subjects', subject_id)

def project_subject_experiments(resource, project_id, subject_id):
    return project_subject(resource, project_id, subject_id)('experiments')

def project_subject_experiment(resource, project_id, subject_id, experiment_id):
    return project_subject(resource, project_id, subject_id)(
        'experiments', experiment_id)

def project_subject_experiment_assessors(
    resource, project_id, subject_id, experiment_id):
    return project_subject(resource, project_id, subject_id)(
        'experiments', experiment_id, 
        'assessors')

def project_subject_experiment_assessor(
    resource, project_id, subject_id, experiment_id, assessor_id):
    return project_subject(resource, project_id, subject_id)(
        'experiments', experiment_id, 
        'assessors', assessor_id)

def project_subject_experiment_status(
    resource, project_id, subject_id, experiment_id):
    return project_subject(resource, project_id, subject_id)(
        'experiments', experiment_id, 
        'status')

@complex_file_id
def project_subject_file(
    resource, project_id, subject_id, file_id):
    return project_subject(resource, project_id, subject_id)(
        'files', *file_id)

def project_subject_resources(
    resource, project_id, subject_id):
    return project_subject(resource, project_id, subject_id)(
        'resources')

def project_subject_resource(
    resource, project_id, subject_id, resource_id):
    return project_subject(resource, project_id, subject_id)(
        'resources', resource_id)

# undocumented in XNAT
def project_subject_resource_files(
    resource, project_id, subject_id, resource_id):
    return project_subject(resource, project_id, subject_id)(
        'resources', resource_id, 
        'files')

@complex_file_id
def project_subject_resource_file(
    resource, project_id, subject_id, resource_id, file_id):
    return project_subject(resource, project_id, subject_id)(
        'resources', resource_id, 
        'files', *file_id)

# ---------------------------------------------------------------------------
# Project Resources: Pipeline Management
# ---------------------------------------------------------------------------

def project_pipelines(resource, project_id):
    return project(resource, project_id)('pipelines')

def project_pipeline(resource, project_id, step_id, experiment_id):
    return project(resource, project_id)('pipelines', step_id, 
                                                 'experiments', experiment_id)

# ---------------------------------------------------------------------------
# Project Resources: Files
# ---------------------------------------------------------------------------

## already defined earlier (defined twice in XNAT documentation)

# ---------------------------------------------------------------------------
# Subject Resources: Files
# ---------------------------------------------------------------------------

## already defined earlier (defined twice in XNAT documentation)

# ---------------------------------------------------------------------------
# Experiment Resources: Files
# ---------------------------------------------------------------------------

@complex_file_id
def project_subject_experiment_file(
    resource, project_id, subject_id, experiment_id, file_id):
    return project_subject_experiment(
        resource, project_id, subject_id, experiment_id)('files', *file_id)

def project_subject_experiment_resources(
    resource, project_id, subject_id, experiment_id):
    return project_subject_experiment(
        resource, project_id, subject_id, experiment_id)('resources')

def project_subject_experiment_resource(
    resource, project_id, subject_id, experiment_id, resource_id):
    return project_subject_experiment(
        resource, project_id, subject_id, experiment_id)('resources', resource_id)

# undocumented
def project_subject_experiment_resource_files(
    resource, project_id, subject_id, experiment_id, resource_id):
    return project_subject_experiment(
        resource, project_id, subject_id, experiment_id)('resources', resource_id, 
                                                         'files')

# undocumented
@complex_file_id
def project_subject_experiment_resource_file(
    resource, project_id, subject_id, experiment_id, resource_id, file_id):
    return project_subject_experiment(
        resource, project_id, subject_id, experiment_id)('resources', resource_id,
                                                         'files', *file_id)


# ---------------------------------------------------------------------------
# Imaging Session Resources
# ---------------------------------------------------------------------------

# Already defined earlier (defined twice in XNAT documentation):

# def project_subject_experiment_assessors():
#     pass

# def project_subject_experiment_assessor():
#     pass


# ---- assessors ---- #

# undocumented
def project_subject_experiment_assessor_files(
    resource, project_id, 
    subject_id, experiment_id, 
    assessor_id, inout):

    return project_subject_experiment_assessor(
        resource, project_id, 
        subject_id, experiment_id, assessor_id)(inout, 'files')

@complex_file_id
def project_subject_experiment_assessor_file(
    resource, project_id, 
    subject_id, experiment_id, 
    assessor_id, inout, file_id):

    return project_subject_experiment_assessor(
        resource, project_id, 
        subject_id, experiment_id, assessor_id)(inout, 'files', *file_id)

def project_subject_experiment_assessor_resources(
    resource, project_id, 
    subject_id, experiment_id, 
    assessor_id, inout):

    return project_subject_experiment_assessor(
         resource, project_id, 
         subject_id, experiment_id, assessor_id)(inout, 'resources')

def project_subject_experiment_assessor_resource(
    resource, project_id, subject_id, 
    experiment_id, assessor_id, 
    inout, resource_id):

    return project_subject_experiment_assessor(
        resource, project_id, 
        subject_id, experiment_id, 
        assessor_id)(inout, 'resources', resource_id)

# undocumented
def project_subject_experiment_assessor_resource_files(
    resource, project_id, subject_id, 
    experiment_id, assessor_id, inout, resource_id):

    return project_subject_experiment_assessor(
         resource, project_id, 
         subject_id, experiment_id, 
         assessor_id)(inout, 'resources', 
                      resource_id, 'files')

@complex_file_id
def project_subject_experiment_assessor_resource_file(
    resource, project_id, subject_id, 
    experiment_id, assessor_id, inout, resource_id, file_id):

    return project_subject_experiment_assessor(
         resource, project_id, 
         subject_id, experiment_id, 
         assessor_id)(inout, 
                      'resources', resource_id, 
                      'files', *file_id)

# ---- reconstructions ---- #

def project_subject_experiment_reconstructions(
    resource, project_id, subject_id, experiment_id):

    return project_subject_experiment(
        resource, project_id, 
        subject_id, experiment_id)('reconstructions')

def project_subject_experiment_reconstruction(
    resource, project_id, subject_id, 
    experiment_id, reconstruction_id):

    return project_subject_experiment(
        resource, project_id, 
        subject_id, experiment_id)(
        'reconstructions', reconstruction_id)

# undocumented
def project_subject_experiment_reconstruction_files(
    resource, project_id, 
    subject_id, experiment_id, 
    reconstruction_id, inout):

    return project_subject_experiment_reconstruction(
        resource, project_id, 
        subject_id, experiment_id, reconstruction_id)(inout, 'files')

@complex_file_id
def project_subject_experiment_reconstruction_file(
    resource, project_id, 
    subject_id, experiment_id, 
    reconstruction_id, inout, file_id):

    return project_subject_experiment_reconstruction(
        resource, project_id, 
        subject_id, experiment_id, reconstruction_id)(inout, 'files', *file_id)

def project_subject_experiment_reconstruction_resources(
    resource, project_id, 
    subject_id, experiment_id, 
    reconstruction_id, inout):

    return project_subject_experiment_reconstruction(
         resource, project_id, 
         subject_id, experiment_id, reconstruction_id)(inout, 'resources')

def project_subject_experiment_reconstruction_resource(
    resource, project_id, subject_id, 
    experiment_id, reconstruction_id, 
    inout, resource_id):

    return project_subject_experiment_reconstruction(
        resource, project_id, 
        subject_id, experiment_id, 
        reconstruction_id)(inout, 'resources', resource_id)

# undocumented
def project_subject_experiment_reconstruction_resource_files(
    resource, project_id, subject_id, 
    experiment_id, reconstruction_id, inout, resource_id):

    return project_subject_experiment_reconstruction(
         resource, project_id, 
         subject_id, experiment_id, 
         reconstruction_id)(inout, 'resources', 
                      resource_id, 'files')

@complex_file_id
def project_subject_experiment_reconstruction_resource_file(
    resource, project_id, subject_id, 
    experiment_id, reconstruction_id, inout, resource_id, file_id):

    return project_subject_experiment_reconstruction(
         resource, project_id, 
         subject_id, experiment_id, 
         reconstruction_id)(inout, 
                      'resources', resource_id, 
                      'files', *file_id)

# ---- scans ---- #

def project_subject_experiment_scans(
    resource, project_id, subject_id, experiment_id):

    return project_subject_experiment(
        resource, project_id, 
        subject_id, experiment_id)('scans')

def project_subject_experiment_scan(
    resource, project_id, subject_id, 
    experiment_id, scan_id):

    return project_subject_experiment(
        resource, project_id, 
        subject_id, experiment_id)(
        'scans', scan_id)

# undocumented
def project_subject_experiment_scan_files(
    resource, project_id, 
    subject_id, experiment_id, 
    scan_id):

    return project_subject_experiment_scan(
        resource, project_id, 
        subject_id, experiment_id, scan_id)('files')

@complex_file_id
def project_subject_experiment_scan_file(
    resource, project_id, 
    subject_id, experiment_id, 
    scan_id, file_id):

    return project_subject_experiment_scan(
        resource, project_id, 
        subject_id, experiment_id, scan_id)('files', *file_id)

def project_subject_experiment_scan_resources(
    resource, project_id, 
    subject_id, experiment_id, 
    scan_id):

    return project_subject_experiment_scan(
         resource, project_id, 
         subject_id, experiment_id, scan_id)('resources')

def project_subject_experiment_scan_resource(
    resource, project_id, subject_id, 
    experiment_id, scan_id, 
    resource_id):

    return project_subject_experiment_scan(
        resource, project_id, 
        subject_id, experiment_id, 
        scan_id)('resources', resource_id)

# undocumented
def project_subject_experiment_scan_resource_files(
    resource, project_id, subject_id, 
    experiment_id, scan_id, resource_id):

    return project_subject_experiment_scan(
         resource, project_id, 
         subject_id, experiment_id, 
         scan_id)('resources', 
                      resource_id, 'files')

@complex_file_id
def project_subject_experiment_scan_resource_file(
    resource, project_id, subject_id, 
    experiment_id, scan_id, resource_id, file_id):

    return project_subject_experiment_scan(
         resource, project_id, 
         subject_id, experiment_id, 
         scan_id)('resources', resource_id, 
                  'files', *file_id)


# ---------------------------------------------------------------------------
# Project Sharing Resources
# ---------------------------------------------------------------------------

def project_subject_experiment_assessor_shares(
    resource, project_id, subject_id, experiment_id, assessor_id):

    return project_subject_experiment_assessor(
        resource, project_id, 
        subject_id, experiment_id, assessor_id)('projects')


def project_subject_experiment_assessor_share(
    resource, project_id, 
    subject_id, experiment_id, 
    assessor_id, other_project_id):

    return project_subject_experiment_assessor(
        resource, project_id, 
        subject_id, experiment_id, assessor_id)('projects', other_project_id)

def project_subject_experiment_shares(
    resource, project_id, subject_id, experiment_id):

    return project_subject_experiment(
        resource, project_id, 
        subject_id, experiment_id)('projects')

def project_subject_experiment_share(
    resource, project_id, 
    subject_id, experiment_id, other_project_id):

    return project_subject_experiment(
        resource, project_id, 
        subject_id, 
        experiment_id)('projects', other_project_id)

def project_subject_shares(
    resource, project_id, subject_id):

    return project_subject(
        resource, project_id, 
        subject_id)('projects')

def project_subject_share(
    resource, project_id, 
    subject_id, other_project_id):

    return project_subject(
        resource, project_id, 
        subject_id)('projects', other_project_id)

# ---------------------------------------------------------------------------
# Archive Resources
# ---------------------------------------------------------------------------

# undocumented
def experiments(resource):
    return resource('archive', 'experiments')

def experiment(resource, experiment_id):
    return resource('archive', 'experiments', experiment_id)

# does not exist
def experiment_assessors(resource, experiment_id):
    return resource('archive', 
                    'experiments', experiment_id, 
                    'assessors')

def experiment_assessor(resource, experiment_id, assessor_id):
    return resource('archive', 
                    'experiments', experiment_id, 
                    'assessors', assessor_id)

# undocumented
def experiment_reconstructions(resource, experiment_id):
    return resource('archive', 
                    'experiments', experiment_id, 
                    'reconstructions')

def experiment_reconstruction(resource, experiment_id, reconstruction_id):
    return resource('archive', 
                    'experiments', experiment_id, 
                    'reconstructions', reconstruction_id)

# undocumented
def experiment_scans(resource, experiment_id):
    return resource('archive', 
                    'experiments', experiment_id, 
                    'scans')

def experiment_scan(resource, experiment_id, scan_id):
    return resource('archive', 
                    'experiments', experiment_id, 
                    'scans', scan_id)

# undocumented
def subjects(resource):
    return resource('archive', 'subjects')

# undocumented
def subject(resource, subject_id):
    return resource('archive', 'subjects', subject_id)

# ------------------------------------------------------ #

def structured_uri(resource, **kwargs):
    sort = [
        'project_id',
        'subject_id',
        'experiment_id',
        'assessor_id',
        'reconstruction_id',
        'scan_id',
        'resource_id',
        'file_id'
        ]

    components = reduce(list.__add__, 
                        [[key.replace('_id', 's'), kwargs[key]]
                         for key in sort if kwargs.has_key(key)])

    return resource(*components)
