import uuid

from nose.tools import raises
from xml.etree import ElementTree as etree

from pyxnat.client import Server
from pyxnat.low.search import get_saved_ids
from pyxnat.low.search import get_saved_results
from pyxnat.low.search import get_saved_document
from pyxnat.low.search import get_data_types
from pyxnat.low.search import get_data_fields
from pyxnat.low.search import get_field_values
from pyxnat.low.search import save
from pyxnat.low.search import delete
from pyxnat.low.search import post
from pyxnat.low.search import is_valid


server = Server('https://nosetests:nosetests@central.xnat.org')
test_search_id = uuid.uuid1().hex
test_data_type = 'xnat:mrSessionData'


def test_get_saved_ids():
    assert get_saved_ids(server) != []


def test_get_saved_results():
    search_id = get_saved_ids(server)[0]['brief_description']
    saved_results = get_saved_results(server, search_id)
    assert saved_results != []


def test_get_saved_document():
    search_id = get_saved_ids(server)[0]['brief_description']
    doc = get_saved_document(server, search_id)
    etree.fromstring(doc)


def test_save():
    columns = ['xnat:mrSessionData/SESSION_ID',
               'xnat:subjectData/SUBJECT_ID']
    filters = [('xnat:mrSessionData/SESSION_ID', 'LIKE', '%'), 'AND']
    saved = save(server, test_search_id, test_data_type,
                 columns, filters, 'testing')
    assert is_valid(server, test_search_id)


def test_delete():
    delete(server, test_search_id)
    assert not is_valid(server, test_search_id)


def test_post():
    columns = ['xnat:mrSessionData/SESSION_ID',
               'xnat:subjectData/SUBJECT_ID']
    filters = [('xnat:mrSessionData/SESSION_ID', 'LIKE', '%'), 'AND']
    docs = post(server, test_data_type, columns, filters)
    assert isinstance(docs, list)
    assert isinstance(docs[0], dict)


def test_mongo_syntax():
    columns = ['xnat:mrSessionData/SESSION_ID',
               'xnat:subjectData/SUBJECT_ID']
    filters = {'xnat:mrSessionData/SESSION_ID': '%'}
    docs = post(server, test_data_type, columns, filters)
    assert isinstance(docs, list)
    assert isinstance(docs[0], dict)


def test_get_data_types():
    data_types = get_data_types(server)
    data_types = [doc['ELEMENT_NAME'] for doc in data_types]
    assert test_data_type in data_types


def test_get_data_fields():
    data_fields = get_data_fields(server, test_data_type)
    data_fields = ['%s/%s' % (test_data_type, doc['FIELD_ID'])
                   for doc in data_fields]
    assert 'xnat:mrSessionData/SESSION_ID' in data_fields


def test_get_field_values():
    field_values = get_field_values(
        server, 'xnat:mrSessionData/SESSION_ID')
    assert 'OAS1_0130_MR1' in field_values
