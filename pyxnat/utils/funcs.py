import pydoc
import functools


def partial(func, *args, **kwargs):
    wrapped = functools.partial(func, *args, **kwargs)
    doc = pydoc.TextDoc().document(func)
    wrapped.__doc__ = doc
    return wrapped
