
def select(data, fields):
    return_dict = False
    if data == list():
        return list()
    if isinstance(data, dict):
        data = [data]
        return_dict = True

    fields = set(data[0].keys()).intersection(fields)

    sub =  [dict(zip(fields, 
                     [row[field] for field in fields])) 
            for row in data]
    if return_dict:
        return sub[0]
    return sub


def identifiers(data):
    return_dict = False
    if data == list():
        return list()
    if isinstance(data, dict):
        data = [data]
        return_dict = True

    fields = [key for key in data[0].keys() if key.endswith('_id')]

    sub =  [dict(zip(fields, 
                     [row[field] for field in fields])) 
            for row in data]
    if return_dict:
        return sub[0]
    return sub


# TODO: change the syntax to be more mongodb-like
def filter(data, field, value):
    return [row for row in data if row.get(field) == value]


def distinct(data, field):
    return sorted(set([
                row.get(field)
                for row in data if row.has_key(field)]))


def limit(data, count):
    return data[:count]


def skip(data, count):
    return data[count:]


class ResultSet(list):

    def __init__(self, data):
        list.__init__(self, data)

    def distinct(self, field):
        return distinct(self, field)

    def filter(self, field, value):
        return ResultSet(filter(self, field, value))

    def select(self, fields):
        return ResultSet(select(self, fields))

    def identifiers(self):
        return ResultSet(identifiers(self))

    def limit(self, count):
        return ResultSet(limit(self, count))

    def skip(self, count):
        return ResultSet(skip(self, count))

    def count(self):
        return len(self)

    def __repr__(self):
        return 'ResultSet(%s)' % list.__repr__(self)
